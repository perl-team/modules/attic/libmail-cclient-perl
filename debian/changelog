libmail-cclient-perl (1.12-12) UNRELEASED; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Xavier Guimard ]
  * Add licenses texts to remove missing-license-text-in-dep5-copyright
    warning

  [ gregor herrmann ]
  * Install debian/tests.txt only when DEB_BUILD_OPTIONS does not contain
    nocheck. Thanks to Thorsten Glaser for the bug report. (Closes:
    #698979)

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Drop obsolete and outdated emacs "Local variables" from
    debian/changelog.
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Add patch by Nicolas Sévelin-Radiguet to fix compilation with clang.
    (Closes: #742431)
  * Bump debhelper compatibility to 9 to be able to use debian/clean and
    get easier hardening flags.
    + Update versioned build-dependency on debhelper accordingly.
  * Add Makefile.old to debian/clean
  * Drop clean target list its files in debian/clean
  * Fixes the following lintian warnings:
    + vcs-field-not-canonical
    + arch-any-package-needs-newer-cdbs
    + copyright-refers-to-symlink-license
    + license-problem-undefined-license

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 06 Jan 2013 22:03:19 +0100

libmail-cclient-perl (1.12-11) unstable; urgency=low

  * Add patch 1002_Don-t-use-PERL_POLLUTE-macros from Niko Tyni to fix
    FTBFS with perl 5.14 (Closes: #628517)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 30 Jul 2011 15:11:15 +0100

libmail-cclient-perl (1.12-10) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Depend on ${misc:Depends}.
  * Update CDBS:
    + Drop local snippets: All in main CDBS now.
    + Include perl-makemaker.mk (not deprecated perlmodule.mk).
    + Tighten build-dependency on cdbs.
    + Slightly relax build-dependency on devscripts.
  * Packaging moved to git (from Subversion):
    + Adjust Vcs-* control file fields.
    + Tidy access rights of installed example files (not source ones).
  * Use source format 3.0 (quilt):
    + Git-ignore quilt .pc dir, and add dpkg-source local-options (to
      ease building with git-buildpackage).
    + Stop including quilt CDBS snippet, and stop build-depending on
      quilt.
  * Update CDBS:
    + Add README.source (replacing README.cdbs-tweaks).
    + Simplify declaring build-dependencies.
  * Bump policy compliance to standards-version 3.9.2.
  * Add licensing header to rules file.
  * Update copyright file:
    + Rewrite using draft 174 of DEP-5 format.
    + Fix add files section for libc-client files (license: Apache-2.0).
  * Fix tighten build-dependency on debhelper.
  * Add git-buildpackage config, enabling signed tags and pristine-tar.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 Jul 2011 02:13:39 +0200

libmail-cclient-perl (1.12-9) unstable; urgency=medium

  * This release (and 1.12-9) fixes FTBFS, and thus closes: bug#487066,
    thanks to Lucas Nussbaum.
  * Bump urgency to medium due to RC bugfix.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 29 jun 2008 10:49:56 +0200

libmail-cclient-perl (1.12-8) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza). Removed: XS-Vcs-Svn fields (source stanza).

  [ Jonas Smedegaard ]
  * Update local cdbs tweaks:
    + Mirror debian/control Vcs-* changes in debian/control.in.
    + Strip non-printable chars from copyright-check.mk output.
    + Relax copyright-check.mk to only warn by default.  This fixes a
      potential FTBFS.
    + Update dependency cleanup to strip cdbs 0.4.27 (not 0.4.27-1).
  * Update debian/copyright-hints.
  * Bump debhelper compatibility level to 6.
  * Semi-auto-update debian/control to update build-dependencies:
      DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes fakeroot debian/rules clean

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 29 jun 2008 10:47:43 +0200

libmail-cclient-perl (1.12-7) unstable; urgency=low

  * Pass over maintenance of the package to the Perl group: Change Maintainer,
    and add myself to Uploaders.

  * Update local cdbs snippets:
    + Fix wget options in update-tarball.
    + Major improvements to copyright-check, including new versioned
      build-dependency on devscripts.  Update debian/copyright_hints.
    + Update debian/README.cdbs-tweaks.
  * Semi-auto-update debian/control to apply changes contained in the above:
    DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes fakeroot debian/rules clean

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 29 Mar 2008 22:44:58 +0100

libmail-cclient-perl (1.12-6) unstable; urgency=low

  * Bump debhelper compat level and build-dependency to version 5.
  * Semi-auto-update debian/control to apply changes contained in the above:
    DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes fakeroot debian/rules clean

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 19 Mar 2008 02:58:35 +0100

libmail-cclient-perl (1.12-5) unstable; urgency=low

  * Add XS-Vcs-Svn and XS-Vcs-Browser fields to debian/control.
  * Move Homepage to own field (from pseudo-field in long description).
  * Bump standards-version to 3.7.3 (no changes needed).
  * Update cdbs tweaks (no canges affecting this packaging currently):
    + Support zip in upstream-tarball.mk
    + Use ~ as repackaging delimiter in upstream-tarball.mk to make room
      for point releases and cleaned up rerelease
    + Rename top srcdir in repackaged tarball to $pkg-$ver.orig to
      comply with Developers Reference 6.7.8.2.
    + Support mangling upstream version string in upstream-tarball.mk
    + Drop buildcore.mk override (set DEB_AUTO_UPDATE_DEBIAN_CONTROL
      manually when needed instead)
  * Adjust watch file to invoke svn-upgrade (not uupdate).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 04 Jan 2008 19:14:17 +0100

libmail-cclient-perl (1.12-4) unstable; urgency=low

  * Build against up-to-date libc-client.
  * Bump standards version to 3.7.2 (no changes needed).
  * Update local cdbs tweaks:
    + Make sure buildinfo ran only once.
    + Add new upstream-tarball.mk to implement get-orig-source and more.
    + Add new copyright-check.mk to help discover changes to copyrights.
    + Add new buildcore.mk overloading upstream buildcore.mk and enables
      debian/control auto-update when DEB_BUILD_OPTIONS contains
      cdbs-autoupdate. Drop local hack using DEB_BUILD_OPTIONS=update.
    + Add README.cdbs-tweaks to source, and advertise in debian/rules.
  * Isolate segfault-fixing patch (bug#308541) using patchsys-quilt.mk.
  * Declare (and strip duplicate) dependencies in debian/rules.
  * Remove Cclient.h in clean target.
  * Mention only copyright and licensing info in debian/copyright (drop
    additional notes, including old FSF address).
  * Semi-auto-update debian/control:
      DEB_BUILD_OPTIONS=cdbs-autoupdate fakeroot debian/rules pre-build

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 16 Sep 2007 20:32:17 +0200

libmail-cclient-perl (1.12-3) unstable; urgency=low

  * Fix GSSAPI by linking explicitly against libc-client. Closes:
    bug#318079 (thanks to Stephen Quinney
    <stephen.quinney@computing-services.oxford.ac.uk>).
  * Switch to using a local cdbs snippet to invoke dh-buildinfo.
  * Raise to standards version 3.6.2 (no changes needed).
  * Use cdbs debian/control auto-build - but only when environment
    includes DEB_BUILD_OPTIONS=update.
  * Auto-update build-dependencies (and manually rip out build-essential
    buggily sneaking in).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 13 Jul 2005 13:08:33 +0200

libmail-cclient-perl (1.12-2) unstable; urgency=low

  * Avoid segfault on some archs. Closes: bug#308541 (thanks to patch by
    Dann Frazier <dannf@hp.com>).

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 10 May 2005 23:57:00 +0200

libmail-cclient-perl (1.12-1) unstable; urgency=high

  * New upstream release.
  * This should work on ia64, so set urgency=high to hopefully reach
    sarge in time for the release.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 26 Oct 2004 14:04:02 +0200

libmail-cclient-perl (1.10-1) unstable; urgency=low

  * New upstream release.
  * Add usage hint to watch file.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Sep 2004 23:13:27 +0200

libmail-cclient-perl (1.9-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version: 3.6.1.
  * Use (and build-depend on) dh-buildinfo.
  * Install generated debian/tests.txt, and remove the file on clean.
  * debian/copyright rewritten:
    + Info duplicated in debian/changelog removed
    + "Linux" no longer mentioned (Debian supports several systems)
    + Simplified section regarding upstream source.
    + Copied copyright and license info verbatim (no real changes except
      updated year to include 2004).
  * Drop the use of compiler flag -fsigned-char (it makes no difference
    on powerpc as intended, and is wrong to use in the first place).

 -- Jonas Smedegaard <dr@jones.dk>  Tue,  4 May 2004 17:19:58 +0200

libmail-cclient-perl (1.8-4) unstable; urgency=low

  * Rebuild once more, to hopefully wakeup ia64 buildd. Hopefully this
    closes: Bug#175765 (if not then please reopen...).
  * This also closes: Bug#236095 (1.8-2 was never succesfully uploaded).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 16 Apr 2004 18:36:03 +0200

libmail-cclient-perl (1.8-3) unstable; urgency=low

  * Rebuild using 'debuild -sa' to include source.

 -- Jonas Smedegaard <dr@jones.dk>  Fri,  5 Mar 2004 04:31:48 +0100

libmail-cclient-perl (1.8-2) unstable; urgency=low

  * Rebuild against (and update dependency against) newest libc-client.
    This closes: #236095 (once it succesfully builds on all platforms).

 -- Jonas Smedegaard <dr@jones.dk>  Thu,  4 Mar 2004 11:49:04 +0100

libmail-cclient-perl (1.8-1) unstable; urgency=low

  * New upstream release.
  * Switch to using CDBS. Build-depend on recent cdbs with proper perl
    support.
  * Build against current libc-client (Closes: Bug#201526).
  * Add watch file (using author subdir).
  * Standards version 3.6.0.
  * Explicitly build-depend on directly dependent stuff again (I have
    been convinced...).
  * Build on all architectures again (that was not the proper approach:
    old broken arm and ia64 builds still avoid the others from entering
    testing...).

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 17 Aug 2003 09:26:03 +0200

libmail-cclient-perl (1.7-5) unstable; urgency=low

  * Add upstream authors to copyright.
  * Limit packaging to platforms passing the tests (and inform upstream
    of the problems with arm and ia64).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 18 Apr 2003 14:23:08 +0200

libmail-cclient-perl (1.7-4) unstable; urgency=low

  * Change section to perl.
  * Update build-dependency on libc-client, thanks to Bdale Garbee
    <bdale@gag.com> (closes: Bug#187087).
  * Tighten build-dependency on libc-client to one with proper
    dependencies, and drop redundant build-dependencies.
  * Lower build-dependency on perl as described in perl-policy 3.3
    (instead of the tighter one used during the 5.8 transition) to ease
    building on woody.
  * Switch debhelper hint from DH_COMPAT to debian/compat, and use v4.
  * Respect noopt build option.
  * Claim compliance with Policy 3.5.9.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Apr 2003 01:12:41 +0200

libmail-cclient-perl (1.7-3) unstable; urgency=low

  * Recompile against new openssl.
  * Change section to interpreters.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 28 Jan 2003 05:23:29 +0100

libmail-cclient-perl (1.7-2) unstable; urgency=low

  * Adopting package, and closes: bug#174094. Thanks, Jaldhar, for
    maintaining it until now.
  * Changing maintainer field.
  * Build against current libc-client package, and build-depend on it.
  * Build with kerberos support, (in line with current libc-client) and
    build-depend on libkrb5-dev.
  * Run `make test` at compile time.
  * Update copyright
    + Extend coverage until the year 2002
    + Mention Debian location of Artistic License and GPL
    + Include a URL for the source (not only the CPAN search engine)
  * Rewrite debian/rules to use debhelper v3 (to support globbing) and
    tighten build-dependency on debhelper.
  * Include examples.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 27 Dec 2002 09:29:33 +0100

libmail-cclient-perl (1.7-1) unstable; urgency=low

  * New upstream release.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Sun, 15 Sep 2002 14:12:29 -0400

libmail-cclient-perl (1.6-1) unstable; urgency=low

  * New upstream release.
  * Added proper pointer to licenses in copyright file. (Closes: #157608)
  * fixed failure when use'ing with qw() (Closes: #160533)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Thu, 12 Sep 2002 14:28:53 -0400

libmail-cclient-perl (1.5-4) unstable; urgency=low

  * Added proper build-depends for perl 5.8.0

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Sat, 17 Aug 2002 22:28:35 -0400

libmail-cclient-perl (1.5-3) unstable; urgency=low

  * Recompiled for perl 5.8.0 

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Thu,  1 Aug 2002 00:50:50 -0400

libmail-cclient-perl (1.5-2) unstable; urgency=high

  * Moved from non-us to main
  * Urgency abused to make sure that happens before woody is released.
  * Fixed mail_append() parameter order which was causing appends to
    IMAP mailboxes not to work and added an imapdate method that returns the 
    date in the format specified by RFC2060 so it can be given to
    mail_append() when appending messages to an IMAP mailbox.  Thanks to 
    GOMBAS Gabor <gombasg@sztaki.hu> for the patch.  (Closes: #144035)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed, 24 Apr 2002 14:57:53 -0400

libmail-cclient-perl (1.5-1) unstable; urgency=low

  * New upstream release.
  * built with the latest libc-client.so, should work correctly now.
    (Closes: #121594)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Mon, 17 Dec 2001 14:15:29 -0500

libmail-cclient-perl (1.4.1-1.1) unstable; urgency=low

  * Just a test to see if the shared c-client is working correctly.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Thu, 13 Dec 2001 16:34:47 -0500

libmail-cclient-perl (1.4.1-1) unstable; urgency=low

  * New upstream release.
  * Fixed build -dependencies (Closes: #118954)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Fri, 16 Nov 2001 00:16:22 -0500

libmail-cclient-perl (1.4-3) unstable; urgency=low

  * Ok, should really go into non-us now.  (Closes: #117578)
  * Makefile.PL was screwed up giving a module that didn't work correctly.
    That's fixed now.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Mon, 29 Oct 2001 15:39:45 -0500

libmail-cclient-perl (1.4-2) unstable; urgency=low

  * NOTE TO FTP MASTERS:  This actually links to a library that contains 
    SSL so it will have to go into non-US. 

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed, 24 Oct 2001 22:12:41 -0400

libmail-cclient-perl (1.4-1) unstable; urgency=low

  * New upstream version.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed, 24 Oct 2001 12:07:01 -0400

libmail-cclient-perl (1.1-1) unstable; urgency=low

  * New maintainer.
  * New upstream version.
  * Built against the latest c-client lib.  (Closes: #98079)
  * Should build properly from source now.  (Closes: #84555, #105147)
  * Updated to latest policy and perl policy. (Closes: #80692, #95413)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Fri,  3 Aug 2001 15:46:36 -0400

libmail-cclient-perl (0.6-4) frozen unstable; urgency=low

  * New upload so it'll go into frozen as well
  * Missed a couple of bugs that should have been closed (closes: bug#48154, bug#51938)

 -- Michael Alan Dorman <mdorman@debian.org>  Wed,  2 Feb 2000 14:50:18 -0500

libmail-cclient-perl (0.6-3) unstable; urgency=low

  * Linked against newer version of c-client lib. (closes: bug#55463)

 -- Michael Alan Dorman <mdorman@debian.org>  Tue, 18 Jan 2000 09:36:11 -0500

libmail-cclient-perl (0.6-2.1) unstable; urgency=low

  * NMU: Now linked against my shared lib version of c-client

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Fri, 22 Oct 1999 22:28:02 -0400

libmail-cclient-perl (0.6-2) unstable; urgency=low

  * Fix binary-arch/indep targets. (closes: bug#42058)

 -- Michael Alan Dorman <mdorman@debian.org>  Wed, 28 Jul 1999 11:21:45 -0400

libmail-cclient-perl (0.6-1) unstable; urgency=low

  * New upstream version.
  * Modified for new perl packages. (closes: bug#41538)
  * Modified to use debhelper.
  * Fixes close on local mailbox (closes: bug#36097)
  * Probably fixes another random segv (closes: bug#41395)

 -- Michael Alan Dorman <mdorman@debian.org>  Sun, 25 Jul 1999 16:16:25 -0400

libmail-cclient-perl (0.4-2) unstable; urgency=low

  * Correct problems spotted by lintian (doh!)

 -- Michael Alan Dorman <mdorman@debian.org>  Fri,  5 Mar 1999 10:35:49 -0500

libmail-cclient-perl (0.4-1) unstable; urgency=low

  * Initial Debianization.

 -- Michael Alan Dorman <mdorman@debian.org>  Thu, 19 Nov 1998 09:27:11 -0500
